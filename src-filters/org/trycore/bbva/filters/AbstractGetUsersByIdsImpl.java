package org.trycore.bbva.filters;

import org.bonitasoft.engine.filter.AbstractUserFilter;
import org.bonitasoft.engine.connector.ConnectorValidationException;

public abstract class AbstractGetUsersByIdsImpl extends AbstractUserFilter {

	protected final static String IDS_INPUT_PARAMETER = "ids";

	protected final java.util.List getIds() {
		return (java.util.List) getInputParameter(IDS_INPUT_PARAMETER);
	}

	@Override
	public void validateInputParameters() throws ConnectorValidationException {
		try {
			getIds();
		} catch (ClassCastException cce) {
			throw new ConnectorValidationException("ids type is invalid");
		}

	}

}
