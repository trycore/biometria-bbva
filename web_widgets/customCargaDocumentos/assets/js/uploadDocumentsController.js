angular.module('bonitasoft.ui')
    .controller('uploadDocumentsController', function (appService, $sce, $q) {
        var ctrl = this;
        ctrl.loopPromises = [];
        ctrl.listDocuments = [];
        ctrl.typeDocument;
        ctrl.loading;

        ctrl.error = false;
        ctrl.errorDesc = "A ocurrido un error";

        ctrl.uploadDocuments = [{
            file: ""
        }]; // lista de archivos a subir 
        ctrl.idDocuments = []; // lista de IDs que devuelve por cada archivo subido
        ctrl.idDocumentFinal = ""; // ID generado al hacer MERGE de los archivos subidos
        ctrl.preview = false;
        ctrl.pdfPage = {
            pageContent: "",
            page: "",
            totalPages: ""
        };

        ctrl.init = function (properties) {
            ctrl.baseUrl = properties.baseUrl;
            ctrl.caseId = properties.caseId;
            ctrl.textButton = properties.textButton;
            ctrl.tirilla = properties.tirilla;
            ctrl.metadata = properties.metadata
        }

        ctrl.addFile = function () {
            ctrl.uploadDocuments.push({
                file: ""
            });
        }

        ctrl.openModal = function (idModal) {
            ctrl.uploadDocuments = [{
                file: ""
            }];

            ctrl.error = false;
            ctrl.preview = false;

            if (ctrl.tirilla == 0) {
                ctrl.getListDocuments();
            }

            $("#" + idModal).modal({
                "backdrop": "static",
                "keyboard": false,
                "show": true
            });
        }

        ctrl.getListDocuments = function () {

            ctrl.loading = true;
            ctrl.error = false;
            ctrl.listDocuments = [];

            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token;

                    appService.getListDocumentsPerProcess(ctrl.baseUrl, ctrl.caseId, token)
                        .then(function (result) {
                            ctrl.loading = false;
                            ctrl.listDocuments = result.data.data.documentTypes;
                        }, function (error) {
                            ctrl.loading = false;
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al listar los documentos";
                        });

                }, function (error) {
                    ctrl.error = true;
                    ctrl.loading = false;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        };

        ctrl.save = function () {

            var metadata = JSON.parse(ctrl.metadata);

            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    let token = result.data.data.token;
                    appService.createDocument(ctrl.baseUrl, token, ctrl.uploadDocuments, metadata)
                        .then(function (result) {
                            ctrl.idDocumentFinal = result.data.data.id;
                            ctrl.getPage(1, ctrl.idDocumentFinal);
                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al generar el documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });

        }

        ctrl.getPage = function (page, documentId) {
            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    token = result.data.data.token;
                    appService.getDocumentPage(ctrl.baseUrl, documentId, token, page)
                        .then(function (result) {
                            ctrl.preview = true;
                            var file = new Blob([result.data], { type: 'application/pdf' });
                            var fileURL = URL.createObjectURL(file);
                            ctrl.pdfPage.pageContent = $sce.trustAsResourceUrl(fileURL);
                            ctrl.pdfPage.page = parseInt(result.headers('page'));
                            ctrl.pdfPage.totalPages = parseInt(result.headers('total-pages'));
                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al obtener el documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        }

        ctrl.previewPage = function () {
            ctrl.getPage(ctrl.pdfPage.page - 1, ctrl.idDocumentFinal);
        }

        ctrl.nextPage = function () {
            ctrl.getPage(ctrl.pdfPage.page + 1, ctrl.idDocumentFinal);
        }

    })