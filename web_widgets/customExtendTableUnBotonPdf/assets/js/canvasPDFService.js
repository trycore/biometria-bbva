angular.module("bonitasoft.ui")
    .service("canvasPDFService", function () {

        var serviceData = {};
        serviceData.pdfFile;
        serviceData.currPageNumber;
        serviceData.zoomed;
        serviceData.context;
        serviceData.fitScale;
        serviceData.canvas;
        serviceData.canvasContainer;
        serviceData.scale;
        serviceData.callbackUpdatePages;


        serviceData.openPage = function (pdfFile, pageNumber) {
            //serviceData.scale = serviceData.zoomed ? serviceData.fitScale : 1.2;
            serviceData.scale = serviceData.fitScale;

            pdfFile.getPage(pageNumber).then(function (page) {

                viewport = page.getViewport(serviceData.scale);
                serviceData.scale = serviceData.canvasContainer.width() / viewport.width;
                viewport = page.getViewport(serviceData.scale);

                // if (serviceData.zoomed) {
                //     serviceData.scale = viewport.height / viewport.width;
                //     viewport = page.getViewport(serviceData.scale);
                // }

                serviceData.canvas.height = viewport.height;
                serviceData.canvas.width = viewport.width;

                var renderContext = {
                    canvasContext: serviceData.context,
                    viewport: viewport
                };

                page.render(renderContext);
                serviceData.callbackUpdatePages();
            });
        };

        serviceData.loadPDF = function (fileURL, canvas, canvasContainer, callbackUpdatePages) {

            if (canvas) {
                console.log("loadingpdf");
                serviceData.canvas = canvas;
                serviceData.canvasContainer = canvasContainer
                serviceData.context = canvas.getContext('2d');
                serviceData.context.clearRect(0, 0, canvas.width, canvas.height);
                serviceData.fitScale = 1.2;
                serviceData.currPageNumber = 1;
                serviceData.zoomed = false;
                serviceData.callbackUpdatePages = callbackUpdatePages;

                PDFJS.disableStream = true;
                PDFJS.getDocument(fileURL).then(function (pdf) {
                    serviceData.pdfFile = pdf;
                    serviceData.openPage(pdf, serviceData.currPageNumber, 1);
                });

            }
        }

        serviceData.toggleZoom = function () {
            serviceData.zoomed = !serviceData.zoomed;
            serviceData.openPage(serviceData.pdfFile, serviceData.currPageNumber);
        };

        serviceData.openNextPage = function () {
            var pageNumber = Math.min(serviceData.pdfFile.numPages, serviceData.currPageNumber + 1);
            if (pageNumber !== serviceData.currPageNumber) {
                serviceData.currPageNumber = pageNumber;
                serviceData.openPage(serviceData.pdfFile, serviceData.currPageNumber);
            }
        };

        serviceData.openPrevPage = function () {
            var pageNumber = Math.max(1, serviceData.currPageNumber - 1);
            if (pageNumber !== serviceData.currPageNumber) {
                serviceData.currPageNumber = pageNumber;
                serviceData.openPage(serviceData.pdfFile, serviceData.currPageNumber);
            }
        };

        return serviceData;

    })