angular.module('bonitasoft.ui')
    .controller('listDocumentsController', function (appService) {
        var ctrl = this;
        ctrl.error = false;
        ctrl.errorDesc = "A ocurrido un error";
        ctrl.loading;
        ctrl.init = function (properties) {
            ctrl.baseUrl = properties.baseUrl;
            ctrl.PayloadEvaluar = properties.PayloadEvaluar;
        }

        ctrl.openModal = function (properties) {
            ctrl.getListDocuments(properties.PayloadEvaluar);
        }

        ctrl.getListDocuments = function (A) {
            ctrl.loading = true;
            ctrl.error = false;
            ctrl.listDocuments = [];
        };
    })