angular.module('bonitasoft.ui')
    .controller('classificationDocumentsController', function (appService, $sce, $q) {
        var ctrl = this;

        ctrl.listDocuments = [];
        ctrl.typeDocument;
        ctrl.dateExpedition;
        ctrl.firstLoad = true;
        ctrl.typify = false;
        ctrl.listAsigned = [];
        ctrl.mergeResult = [];
        ctrl.loading;

        ctrl.error = false;
        ctrl.errorDesc = "A ocurrido un error";

        ctrl.pdfPage = {
            pageContent: "",
            page: "",
            totalPages: ""
        };

        ctrl.init = function (properties) {
            ctrl.baseUrl = properties.baseUrl;
            ctrl.caseId = properties.caseId;
            ctrl.textButton = properties.textButton;
            ctrl.documentId = properties.documentId;
        }

        ctrl.initDatePicker = function () {
            $('.form_date').datetimepicker({
                language: 'es',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                minView: 2,
                forceParse: 0,
                inline: true,
                pickerPosition: "top-left"
            });
        }

        ctrl.openModal = function (idModal) {

            ctrl.error = false;

            ctrl.getListDocuments();
            ctrl.getPage(1);

            $("#" + idModal).modal({
                "backdrop": "static",
                "keyboard": false,
                "show": true
            });
        }

        ctrl.getListDocuments = function () {

            ctrl.loading = true;
            ctrl.error = false;
            ctrl.listDocuments = [];

            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token

                    appService.getListDocumentsPerProcess(ctrl.baseUrl, ctrl.caseId, token)
                        .then(function (result) {
                            ctrl.loading = false;
                            ctrl.listDocuments = result.data.data.documentTypes;
                        }, function (error) {
                            ctrl.loading = false;
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al listar los documentos";
                        });

                }, function (error) {
                    ctrl.loading = false;
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        };

        ctrl.getPage = function (page) {
            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    token = result.data.data.token;

                    appService.getDocumentPage(ctrl.baseUrl, ctrl.documentId, token, page)
                        .then(function (result) {
                            ctrl.preview = true;
                            var file = new Blob([result.data], { type: 'application/pdf' });
                            var fileURL = URL.createObjectURL(file);
                            ctrl.pdfPage.pageContent = $sce.trustAsResourceUrl(fileURL);
                            ctrl.pdfPage.page = parseInt(result.headers('page'));
                            ctrl.pdfPage.totalPages = parseInt(result.headers('total-pages'));

                            if (ctrl.firstLoad) {
                                for (var i = 1; i <= ctrl.pdfPage.totalPages; i++) {
                                    ctrl.listAsigned.push(
                                        {
                                            type: "",
                                            dateExpedition: "",
                                            asigned: false
                                        }
                                    );
                                }
                                ctrl.firstLoad = false;
                            }

                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al obtener el documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        }

        ctrl.toAssign = function () {
            ctrl.listAsigned[ctrl.pdfPage.page - 1].type = ctrl.typeDocument;
            ctrl.listAsigned[ctrl.pdfPage.page - 1].dateExpedition = ctrl.dateExpedition;
            ctrl.listAsigned[ctrl.pdfPage.page - 1].asigned = true;
            ctrl.typeDocument = "";
            ctrl.dateExpedition = "";
            ctrl.validateTypify();
        }

        ctrl.modify = function () {
            ctrl.listAsigned[ctrl.pdfPage.page - 1].asigned = false;
            ctrl.validateTypify();
        }

        ctrl.validateTypify = function () {
            var valid = true;
            for (var i = 0; i < ctrl.listAsigned.length; i++) {
                if (!ctrl.listAsigned[i].asigned) {
                    valid = false;
                    break;
                }
            }
            if (valid) {
                ctrl.typify = true;
            } else {
                ctrl.typify = false;
            }
        }

        ctrl.typifying = function () {

            var data = {
                type: "PP"
            };

            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token;

                    appService.splitDocument(ctrl.baseUrl, data, token, ctrl.documentId)
                        .then(function (result) {
                            var map = {};

                            for (i = 0; i < ctrl.listAsigned.length; i++) {
                                if (map[ctrl.listAsigned[i].type.trim()]) {
                                    map[ctrl.listAsigned[i].type.trim()].push(
                                        { id: result.data.data[i].id }
                                    );
                                } else {
                                    map[ctrl.listAsigned[i].type.trim()] = [];
                                    map[ctrl.listAsigned[i].type.trim()].push(
                                        { id: result.data.data[i].id }
                                    );
                                }
                            }
                            console.log("map")
                            console.log(map)

                            for (var clave in map) {
                                console.log("CLAVE")
                                console.log(clave)
                                ctrl.loopMerge(map, clave);
                            }

                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al hacer split del documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        }

        ctrl.previewPage = function () {
            ctrl.getPage(ctrl.pdfPage.page - 1);
        }

        ctrl.nextPage = function () {
            ctrl.getPage(ctrl.pdfPage.page + 1);
        }

        ctrl.loopMerge = function (map, clave) {
            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token
                    if (map.hasOwnProperty(clave)) {
                        console.log("inicio log")
                        console.log(map[clave].length > 1)
                        // if (map[clave].length > 1) {
                            console.log("EN EL MERGE")
                            appService.mergeDocument(ctrl.baseUrl, ctrl.caseId, token, map[clave])
                                .then(function (result) {
                                    console.log("result merge ")
                                    console.log(result.data)
                                    ctrl.patch(result.data.data.id, clave)
                                }, function (error) {
                                    ctrl.error = true;
                                    ctrl.errorDesc = "A ocurrido un error al mezclar los documentos";
                                });

                        // } else {
                        //     ctrl.patch(map[clave][0].id, clave)
                        // }
                    }
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        }

        ctrl.loopPatch = function (i) {
            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token
                    var data = {
                        "categorization": {
                            "documentTypeId": ctrl.mergeResult[i].value
                        }
                    };

                    appService.updateDocument(ctrl.baseUrl, ctrl.mergeResult[i].id, data, token)
                        .then(function (result) {

                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al actualizar el documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                })
        }

        ctrl.patch = function (id, value) {
            console.log("EN EL PATCH")
            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token
                    var data = {
                        "categorization": {
                            "documentTypeId": value
                        }
                    }
                    appService.updateDocument(ctrl.baseUrl, id, data, token)
                        .then(function (result) {

                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al actualizar el documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                })
        }
    })