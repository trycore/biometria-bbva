angular.module("bonitasoft.ui")
    .service("appService", function ($http, Upload) {
        var url = ""

        this.grantingTicket = function (BASE_URL, USERGT, AAPGT, iv_ticketService) {
            url = BASE_URL + "/TechArchitecture/co/grantingTicket/V02";

            data = {
                "authentication": {
                    "userID": USERGT,//usuario de caja. usuario banco. es el CE
                    "consumerID": AAPGT,//el canal. oficina, bpm o movil.
                    "authenticationType": "00",//valor fijo. pero tienen que confirmar con arquitectura
                    "authenticationData": [
                        {
                            "idAuthenticationData": iv_ticketService,//siempre es iv ticket. es fijo. tienen que confirmar con arquitectura. puede que sea variable
                            "authenticationData": [// tienen que confirmar con arquitectura. puede que sea variable
                                "EKIJYXDP4EisZvsqsmyM7VNrV8j48appxBjAawb5n6hF1e1pqkhuXw"
                            ]
                        }
                    ]
                },
                "backendUserRequest": {
                    "userId": "",
                    "accessCode": "",
                    "dialogId": ""
                }
            };

            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "Access-Control-Allow-Origin": "*"
                }
            };

            return $http.post(url, data, header);
        };

        this.generatePkcs7 = function (BASE_URL, TSEC, AAPGT) {//unicamente para la capa local.
            url = BASE_URL + "/singleSignOn/V01";

            var data = {
                "aap": AAPGT,//fijo dependiendo del canal
                "identityDocumentNumber": "63323159",//tienen que revisarlo con arquitectura
                "cypherType": "10"//valor fijo
            }

            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "tsec": TSEC
                }
            };
            return $http.post(url, data, header);
        };

        this.generateToken = function (BASE_URL, pkcs7, AAPGT, CH_NAME, CH_PASS) {
            url = BASE_URL + "/AUTHDM/token/generate";

            var fd = new FormData();
            fd.append("pkcs7", pkcs7);
            fd.append("request", JSON.stringify({
                "aap": AAPGT,
                "endUser": "C999998",//la persona que está logueada. en bpm es el usuario maquina.
                "channel": {
                    "name": CH_NAME,//fijos dependiendo del canal que se utilice
                    "password": CH_PASS//fijos dependiendo del canal que se utilice
                }
            }))
            var header = {
                headers: {
                    "Content-Type": undefined
                }
            };
            return $http.post(url, fd, header);
        };

        this.uploadFile = function (BASE_URL, token, file) {
            var url = BASE_URL + "/Downup/documents/v0/file";
            var upload = Upload.upload({
                url: url,
                method: 'POST',
                headers: { "local-access-token": token, "Content-Type": "application/x-www-form-urlencoded" },
                data: {
                    file: file
                }
            });
            return upload;
        }

        this.uploadDocument = function (BASE_URL, token, file, data) {
            url = BASE_URL + "/DOCCLA/uploadDocument";

            var fd = new FormData();
            
            fd.append("file", file)
            fd.append("data", JSON.stringify(data))
            
            var header = {
                headers: { 
                    "local-access-token": token, 
                    "Content-Type": "multipart/form-data" 
                }
            };

            return $http.post(url, fd, header);

        }

        this.uploadDocumentBackup = function (BASE_URL, token, file, data) {
            url = BASE_URL + "/DOCCLA/uploadDocument";

            var upload = Upload.upload({
                url: url,
                method: 'POST',
                headers: { "local-access-token": token, "Content-Type": "multipart/form-data" },
                data: {
                    file: file,
                    data: data
                }
            });
            return upload;
        }

        this.createDocument = function (BASE_URL, tsec, data) {
            url = BASE_URL + "/documents/v0/documents";

            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "tsec": tsec
                }
            };
            return $http.post(url, data, header);
        }

        this.linkFile = function (BASE_URL, tsec, documentId, fileId) {
            url = `${BASE_URL}/documents/v0/documents/${documentId}/file/${fileId}`

            var data = {};

            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json",
                    "tsec": tsec
                }
            };
            return $http.put(url, data, header);
        }
        
        this.downloadFileToView = function (BASE_URL, token, fileId) {
            url = `${BASE_URL}/Downup/documents/v0/documents/${fileId}/file`

            var header = {
                headers: {
                    "local-access-token": token,
                    "Accept": "multipart/form-data"
                },
                "responseType": "arraybuffer"
            };
            return $http.get(url, header);
        }

        this.createLogicalFolder = function (BASE_URL, tsec, data) {
            url = BASE_URL + "/logical-folders/v0/logical-folders";

            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "tsec": tsec
                }
            };
            return $http.post(url, data, header);
        }

        this.modifyChildrenLogicalFolder = function (BASE_URL, tsec, fileId, folderId) {
            url = `${BASE_URL}/logical-folders/v0/logical-folders/${folderId}/children/${fileId}`

            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "accept": "application/json",
                    "tsec": tsec
                }
            }

            var data = {
                "id": fileId,
                "nodeType": {
                    "description": "DOCUMENT",
                    "id": "DOCUMENT"
                }
            }

            return $http.put(url, data, header)

        }

        this.downloadFile = function (BASE_URL, token, fileId) {
            url = `${BASE_URL}/Downup/documents/v0/documents/${fileId}/file`

            var header = {
                headers: {
                    "local-access-token": token,
                    "Accept": "multipart/form-data"
                }
            };
            return $http.get(url, header);
        }

        this.downloadFile_XMLHttpRequest = function (BASE_URL, token, fileId) {
            return new Promise(function (resolve, reject) {
                var xhr = new XMLHttpRequest();
                xhr.open('GET', `${BASE_URL}/Downup/documents/v0/documents/${fileId}/file`, true);
                xhr.setRequestHeader("local-access-token", token)
                xhr.setRequestHeader("Accept", "multipart/form-data")
                xhr.responseType = 'arraybuffer';
                xhr.onload = function (e) {
                    if (this.status == 200) {
                        var blob = new Blob([this.response], { type: "application/pdf" });
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = "DocTirilla" + new Date() + ".pdf";
                        link.click();
                    }
                };
                // xhr.onload = resolve;
                // xhr.onerror = reject;
                return xhr.send();
            })
        }

        this.smartList = function (BASE_URL, tsec, processId) {
            url = `${BASE_URL}/document-processes/v0/document-processes/${processId}/definitions`
            var header = {
                headers: {
                    "Content-Type": "application/json",
                    "Accept": "application/json",
                    "tsec": tsec
                }
            };
            return $http.get(url, header);
        }

        this.listDocuments = function (BASE_URL, token, userType, userValue, processId, documentId) {

            url = BASE_URL + "/DOCCLA/listDocuments?"
            documentId ? url += `id=${documentId}&` : ''
            userType ? url += `userIdType=${userType}&` : ''
            userValue ? url += `userIdValue=${userValue}&` : ''
            processId ? url += `processId=${processId}&` : ''

            var header = {
                headers: {
                    "local-access-token": token,
                    "Content-Type": "application/json"
                }
            };
            return $http.get(url, header);
        }

        this.getPage = function (BASE_URL, token, documentId, page) {
            url = BASE_URL + "/DOCCLA/getPage?id=" + documentId + "&page=" + page;

            var header = {
                headers: {
                    "local-access-token": token,
                    "Accept": "multipart/form-data"
                },
                responseType: "arraybuffer"
            };
            return $http.get(url, header);
        }

        this.classifyDocuments = function (BASE_URL, token, data) {
            url = BASE_URL + "/DOCCLA/classifyDocuments";

            var header = {
                headers: {
                    "local-access-token": token,
                    "Content-Type": "application/json"
                }
            };
            return $http.post(url, data, header);
        }

        this.uploadImageDocument = function (BASE_URL, token, file) {
            url = BASE_URL + "/document-svc/uploadDocumentMobile";

            var data = {
                file: file
            };

            var header = {
                headers: {
                    "local-access-token": token,
                    "Accept": "multipart/form-data"
                }
            };
            return $http.post(url, data, header);
        }

        this.downloadTmpFile = function (BASE_URL, token, fileId) {
            url = BASE_URL + "/document-svc/download/" + 1;

            var header = {
                headers: {
                    "local-access-token": token,
                    "Accept": "multipart/form-data"
                }
            };
            return $http.get(url, header);
        }

    })