angular.module('bonitasoft.ui')
    .controller('documentManagerController', function ($scope, $sce, $http) {

        var ctrl = this;
        // variables
        ctrl.url = "";
        ctrl.headers = {};
        ctrl.dataToSend = {};
        ctrl.errorMessage = "";
        ctrl.pdfContent = "";
        
        // Fin variables de configuración
        ////////////////////////////////////////

        ctrl.init = function () {
            ctrl.url = $scope.properties.url;
            ctrl.headers = $scope.properties.headers;
            ctrl.dataToSend = $scope.properties.dataToSend;
            ctrl.pdfContent = "";
        };

        ctrl.getContent = function () {
           
           
            // Se asigna nuevamente el valor a la propiedad url para
            // evitar que quede como undefined
            ctrl.url = $scope.properties.url;
            
            // Se asigna nuevamente el valor a la propiedad dataToSend para
            // evitar que se pierdan valores que se deben enviar al servicio.
            ctrl.dataToSend = $scope.properties.dataToSend;
            
            // Elimina el contenido actual en el visor de PDFs
            ctrl.pdfContent = "";
            $("#pdfObject").attr("data", "");
            $("#pdfObject").removeData();
            $("#pdfEmbed").attr("src", "");
            
            $http.post(ctrl.url, ctrl.dataToSend, ctrl.headers)
                .then(function (result) {
                    var file = new Blob([result.data], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
                    ctrl.pdfContent = $sce.trustAsResourceUrl(fileURL);
                    
                    // Se codifica en base64 el contenido del PDF
                    var text = result.data;
                    var binary = '';
                    var bytes = new Uint8Array(text);
                    var len = bytes.byteLength;
                    for (var i = 0; i < len; i++) {
                        binary += String.fromCharCode(bytes[i]);
                    }
                    var encodedString = window.btoa(binary);
                    $("#pdfEmbed").attr("src", "data:application/pdf;base64," + encodedString);
                    
                }, function (error) {
                    ctrl.errorMessage = "inlinePDFViewer - Ha ocurrido un error";
                    console.log("inlinePDFViewer - Error getContent", error);
                });
        };

    })