angular.module('bonitasoft.ui')
    .controller('listDocumentsController', function (appService) {
        var ctrl = this;
        ctrl.listDocuments = [];
        ctrl.error = false;
        ctrl.errorDesc = "A ocurrido un error";
        ctrl.loading;

        ctrl.init = function (properties) {
            ctrl.baseUrl = properties.baseUrl;
            ctrl.caseId = properties.caseId;
            ctrl.textButton = properties.textButton;
        }

        ctrl.openModal = function (idModal) {
            ctrl.getListDocuments();

            $("#" + idModal).modal({
                "backdrop": "static",
                "keyboard": false,
                "show": true
            });
        }

        ctrl.getListDocuments = function () {

            ctrl.loading = true;

            ctrl.error = false;
            ctrl.listDocuments = [];

            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {

                    var token = result.data.data.token;
                    appService.getListDocumentsPerProcess(ctrl.baseUrl, ctrl.caseId, token)
                        .then(function (result) {
                            ctrl.listDocuments = result.data.data.documentTypes;
                            ctrl.loading = false;
                        }, function (error) {
                            ctrl.loading = false;
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al listar los documetos";
                        });

                }, function (error) {
                    ctrt.loading = false;
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                    ctrl.openModal("listDocumentsModal");
                });
        };
    })