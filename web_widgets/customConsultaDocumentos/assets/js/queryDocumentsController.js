angular.module('bonitasoft.ui')
    .controller('queryDocumentsController', function (appService, $sce) {

        var ctrl = this;

        ctrl.listDocuments = [];
        ctrl.listDocumentsUploaded = [];
        ctrl.error = false;
        ctrl.errorDesc = "A ocurrido un error";
        ctrl.documentSelected;
        ctrl.preview = false;
        ctrl.pdfPage = {
            pageContent: "",
            page: "",
            totalPages: ""
        };
        ctrl.loading;

        ctrl.init = function (properties) {
            ctrl.baseUrl = properties.baseUrl;
            ctrl.caseId = properties.caseId;
            ctrl.textButton = properties.textButton;
            ctrl.participantKey = properties.participantKey;
            ctrl.documentTypeId = properties.documentTypeId;
        }

        ctrl.openModal = function (idModal) {

            ctrl.error = false;
            ctrl.file = null;
            ctrl.preview = false;

            ctrl.getListDocuments();

            $("#" + idModal).modal({
                "backdrop": "static",
                "keyboard": false,
                "show": true
            });
        }

        
        ctrl.openInternalModal = function (idModal) {

            $("#" + idModal).modal({
                "backdrop": "static",
                "keyboard": false,
                "show": true
            });
        }

        ctrl.getListDocuments = function () {
            ctrl.loading = true;
            ctrl.error = false;
            ctrl.listDocuments = [];

            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    var token = result.data.data.token;

                    appService.getListDocumentsPerProcess(ctrl.baseUrl, ctrl.caseId, token)
                        .then(function (result) {
                            ctrl.listDocuments = result.data.data.documentTypes;

                            appService.generateAccesToken(ctrl.baseUrl)
                                .then(function (result) {
                                    var token = result.data.data.token;

                                    appService.getListDocuments(ctrl.baseUrl, ctrl.caseId, ctrl.participantKey, ctrl.documentTypeId, token)
                                        .then(function (result) {
                                            ctrl.loading = false;
                                            ctrl.listDocumentsUploaded = result.data.data;

                                            for (i = 0; i < ctrl.listDocuments.length; i++) {
                                                ctrl.listDocuments[i].loaded = false;
                                                for (j = 0; j < ctrl.listDocumentsUploaded.length; j++) {
                                                    if (ctrl.listDocuments[i].documentTypeId == ctrl.listDocumentsUploaded[j].categorization.documentTypeId) {
                                                        ctrl.listDocuments[i].loaded = true;
                                                        ctrl.listDocuments[i].documentId = ctrl.listDocumentsUploaded[j].id;
                                                        ctrl.listDocuments[i].documentInfo = ctrl.listDocumentsUploaded[j].documentInfo;
                                                        break;
                                                    }
                                                }
                                            }
                                        }, function (error) {
                                            ctrl.error = true;
                                            ctrl.loading = false;
                                            ctrl.errorDesc = "A ocurrido un error al listar los documentos cargados";
                                        });

                                }, function (error) {
                                    ctrl.error = true;
                                    ctrl.loading = false;
                                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                                });
                        }, function (error) {
                            ctrl.error = true;
                            ctrl.loading = false;
                            ctrl.errorDesc = "A ocurrido un error al listar los documentos";
                        });

                }, function (error) {
                    ctrl.error = true;
                    ctrl.loading = false;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        };

        ctrl.viewInformation = function (document) {
            ctrl.documentSelected = document;
            ctrl.openInternalModal('viewDocumentsInfoModal');
            ctrl.getPage(1, ctrl.documentSelected.documentId);
        }

        ctrl.getPage = function (page, documentId) {
            appService.generateAccesToken(ctrl.baseUrl)
                .then(function (result) {
                    token = result.data.data.token;
                    appService.getDocumentPage(ctrl.baseUrl, documentId, token, page)
                        .then(function (result) {
                            ctrl.preview = true;
                            var file = new Blob([result.data], { type: 'application/pdf' });
                            var fileURL = URL.createObjectURL(file);
                            ctrl.pdfPage.pageContent = $sce.trustAsResourceUrl(fileURL);
                            ctrl.pdfPage.page = parseInt(result.headers('page'));
                            ctrl.pdfPage.totalPages = parseInt(result.headers('total-pages'));
                        }, function (error) {
                            ctrl.error = true;
                            ctrl.errorDesc = "A ocurrido un error al obtener el documento";
                        });
                }, function (error) {
                    ctrl.error = true;
                    ctrl.errorDesc = "A ocurrido un error al generar el token";
                });
        }

        ctrl.previewPage = function () {
            ctrl.getPage(ctrl.pdfPage.page - 1, ctrl.documentSelected.documentId);
        }

        ctrl.nextPage = function () {
            ctrl.getPage(ctrl.pdfPage.page + 1, ctrl.documentSelected.documentId);
        }

    })