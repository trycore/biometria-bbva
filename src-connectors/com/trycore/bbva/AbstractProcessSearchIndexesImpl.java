package com.trycore.bbva;

import org.bonitasoft.engine.connector.AbstractConnector;
import org.bonitasoft.engine.connector.ConnectorValidationException;

public abstract class AbstractProcessSearchIndexesImpl extends
		AbstractConnector {

	protected final static String CASO_INPUT_PARAMETER = "caso";
	protected final static String CLAVE_INPUT_PARAMETER = "clave";
	protected final static String VALOR_INPUT_PARAMETER = "valor";
	protected final static String ENTIDAD_INPUT_PARAMETER = "entidad";
	protected final String RESULTADO_OUTPUT_PARAMETER = "resultado";

	protected final java.lang.Long getCaso() {
		return (java.lang.Long) getInputParameter(CASO_INPUT_PARAMETER);
	}

	protected final java.lang.String getClave() {
		return (java.lang.String) getInputParameter(CLAVE_INPUT_PARAMETER);
	}

	protected final java.lang.String getValor() {
		return (java.lang.String) getInputParameter(VALOR_INPUT_PARAMETER);
	}

	protected final java.lang.String getEntidad() {
		return (java.lang.String) getInputParameter(ENTIDAD_INPUT_PARAMETER);
	}

	protected final void setResultado(java.lang.Object resultado) {
		setOutputParameter(RESULTADO_OUTPUT_PARAMETER, resultado);
	}

	@Override
	public void validateInputParameters() throws ConnectorValidationException {
		try {
			getCaso();
		} catch (ClassCastException cce) {
			throw new ConnectorValidationException("caso type is invalid");
		}
		try {
			getClave();
		} catch (ClassCastException cce) {
			throw new ConnectorValidationException("clave type is invalid");
		}
		try {
			getValor();
		} catch (ClassCastException cce) {
			throw new ConnectorValidationException("valor type is invalid");
		}
		try {
			getEntidad();
		} catch (ClassCastException cce) {
			throw new ConnectorValidationException("entidad type is invalid");
		}

	}

}
