/**
 * 
 */
package com.trycore.bbva;
import org.bonitasoft.engine.api.BusinessDataAPI;
import org.bonitasoft.engine.connector.ConnectorException;

import com.bbva.model.ProcessSearchIndexes;
import com.bonitasoft.engine.api.APIAccessor;

/**
 *The connector execution will follow the steps
 * 1 - setInputParameters() --> the connector receives input parameters values
 * 2 - validateInputParameters() --> the connector can validate input parameters values
 * 3 - connect() --> the connector can establish a connection to a remote server (if necessary)
 * 4 - executeBusinessLogic() --> execute the connector
 * 5 - getOutputParameters() --> output are retrieved from connector
 * 6 - disconnect() --> the connector can close connection to remote server (if any)
 */
public class ProcessSearchIndexesImpl extends AbstractProcessSearchIndexesImpl {

	@Override
	protected void executeBusinessLogic() throws ConnectorException{
		//Get access to the connector input parameters
		//getCaso();
		//getClave();
		//getValor();
		//getEntidad();	
		
		
		
		
		
		ProcessSearchIndexes ps = new ProcessSearchIndexes();
		ps.setId_caso(getCaso());
		ps.setClave(getClave());
		ps.setValor(getValor());
		ps.setEntidad(getEntidad());		
		setResultado(ps);
	
		//TODO execute your business logic here 
	
		//WARNING : Set the output of the connector execution. If outputs are not set, connector fails
		//setResultado(resultado);
	
	 }

	@Override
	public void connect() throws ConnectorException{
		//[Optional] Open a connection to remote server
	
	}

	@Override
	public void disconnect() throws ConnectorException{
		//[Optional] Close connection to remote server
	
	}

}
